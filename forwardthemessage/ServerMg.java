package net.rstvvoli.forwardthemessage;

import net.rstvvoli.sockets.SqServer;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Logger;

@SuppressWarnings("InfiniteLoopStatement")
public class ServerMg {

    private static final Logger LOG = Logger.getLogger(SqServer.class.getName());

    public static void main(String[] args) throws IOException {
        LOG.info("Сервер запущен ");

        try (ServerSocket serverSocket = new ServerSocket(1337)) {
            while (true) {
                Socket socket = serverSocket.accept();
                serverClient(socket);
            }
        }
    }

    private static void serverClient(Socket socket) throws IOException {
        LOG.info("Подклчен клиент: " + socket.getInetAddress());

        String message = null;
        byte[] byteMessage = new byte[256];

        try(InputStream inputStream = socket.getInputStream();
            OutputStream outputStream = socket.getOutputStream()) {

            while (message == null) {
                inputStream.read(byteMessage);
                message = bytesToString(byteMessage);
            }
            System.out.println("Клиент прислал: " + message);
            outputStream.write(stringToBytes(message));
        }
    }

    /**
     * Метод преобразует строковое сообщение в массив байтов;
     * @param message сообщение;
     * @return байты;
     */
    private static byte[] stringToBytes(String message) {
        return message.getBytes();
    }

    /**
     * Метод преобразует массив байтов строковое ообщение;
     * @param byteArray массив байтов;
     * @return сообщение;
     */
    private static String bytesToString(byte[] byteArray){
        return new String(byteArray);
    }
}