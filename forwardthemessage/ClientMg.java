package net.rstvvoli.forwardthemessage;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;

public class ClientMg {
    private static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        
        try (Socket socket = new Socket("localhost", 1337);
             OutputStream outputStream = socket.getOutputStream();
             InputStream inputStream = socket.getInputStream()) {

            System.out.print("Введите сообщение, которое необходимо вывести на сервере: ");
            String message = sc.nextLine();
            
            byte[] byteMessage = new byte[256];
            byteMessage = message.getBytes();
            outputStream.write(byteMessage);

            inputStream.read(byteMessage);
            System.out.println("Сервер прислал: " + "Сам " + bytesToString(byteMessage));
        }
    }

    /**
     * Метод преобразует массив байтов строковое оообщение;
     * @param byteArray массив байтов;
     * @return строковое сообщение;
     */
    private static String bytesToString(byte[] byteArray){
        return new String(byteArray);
    }
}